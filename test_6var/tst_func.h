#ifndef TESTPROJECT_H
#define TESTPROJECT_H

#include <QtCore>
#include <QtTest/QtTest>

class TestProject : public QObject
{
Q_OBJECT

public:
TestProject();

private slots:

void Test_case1();
void Test_case2();
void Test_case3();
void Test_case4();
void Test_case5();
void Test_case6();
};

#endif // TESTPROJECT_H
