#include <QtTest>
#include <../func.h>
#include <tst_func.h>

TestProject::TestProject()
{

}

void TestProject::Test_case1()
{
int test_n_before = 111;
function(test_n_before);
QCOMPARE(7, test_n_before);

test_n_before = 110;
function(test_n_before);
QCOMPARE(6, test_n_before);
}

void TestProject::Test_case2()
{
int test_n_before = 190;
function1(test_n_before);
QCOMPARE(10111110, test_n_before);

test_n_before = 10;
function1(test_n_before);
QCOMPARE(1010, test_n_before);
}

void TestProject::Test_case3()
{
int test_n_before = 111;
function2(test_n_before);
QCOMPARE(7, test_n_before);

test_n_before = 110;
function2(test_n_before);
QCOMPARE(6, test_n_before);
}

void TestProject::Test_case4()
{
int test_n_before = 17;
function3(test_n_before);
QCOMPARE(1111, test_n_before);

test_n_before = 25;
function3(test_n_before);
QCOMPARE(10101, test_n_before);
}

void TestProject::Test_case5()
{
int test_n_before = 11;
function4(test_n_before);
QCOMPARE(9, test_n_before);

test_n_before = 10;
function4(test_n_before);
QCOMPARE(8, test_n_before);
}


void TestProject::Test_case6()
{
int test_n_before = 22;
function5(test_n_before);
QCOMPARE(26, test_n_before);

test_n_before = 15;
function5(test_n_before);
QCOMPARE(17, test_n_before);
}

