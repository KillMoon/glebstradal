#include "func.h"
#include <cmath>
#include <iostream>
using namespace std;

//convert 2 to 10
void function(int &n)
{
    int r1 = 2; int  r2 = 10;
    int k, res;
    k = 0; res = 0;

    while (n > 0)
    {
        res += n % 10 * pow(r1, k);
        k++;
        n /= 10;
    }

    k = 1; n = 0;
    while (res > 0)
    {
        n += res % r2 * k;
        k *= 10;
        res /= r2;
    }
    cout << n;
}
//convert 10 to 2
void function1(int &n)
{
    int r1 = 10; int  r2 = 2;
    int k, res;
    k = 0; res = 0;

    while (n > 0)
    {
        res += n % 10 * pow(r1, k);
        k++;
        n /= 10;
    }

    k = 1; n = 0;
    while (res > 0)
    {
        n += res % r2 * k;
        k *= 10;
        res /= r2;
    }
}
//convert 2 to 8
void function2(int &n)
{
    int r1 = 2; int  r2 = 8;
    int k, res;
    k = 0; res = 0;

    while (n > 0)
    {
        res += n % 10 * pow(r1, k);
        k++;
        n /= 10;
    }

    k = 1; n = 0;
    while (res > 0)
    {
        n += res % r2 * k;
        k *= 10;
        res /= r2;
    }
}
//convert 8 to 2
void function3(int &n)
{
    int r1 = 8; int  r2 = 2;
    int k, res;
    k = 0; res = 0;

    while (n > 0)
    {
        res += n % 10 * pow(r1, k);
        k++;
        n /= 10;
    }

    k = 1; n = 0;
    while (res > 0)
    {
        n += res % r2 * k;
        k *= 10;
        res /= r2;
    }
}
//convert 8 to 16
void function4(int &n)
{
    int r1 = 8; int r2 = 10;
    int k, res;
    k = 0; res = 0;

    while (n > 0)
    {
        res += n % 10 * pow(r1, k);
        k++;
        n /= 10;
    }

    k = 1; n = 0;
    while (res > 0)
    {
        n += res % r2 * k;
        k *= 10;
        res /= r2;
    }
}

//convert 16 to 8
void function5(int &n)
{
    int r1 = 10; int r2 = 8;
    int k, res;
    k = 0; res = 0;

    while (n > 0)
    {
        res += n % 10 * pow(r1, k);
        k++;
        n /= 10;
    }

    k = 1; n = 0;
    while (res > 0)
    {
        n += res % r2 * k;
        k *= 10;
        res /= r2;
    }
}

//1
//2
//3
//4
//5
//6
//7
//8
//9
//10
//11
//12
